import ROOT as R
import sys

#path = '../LHE/HH_PWG_FT/'
#path = '../LHE/HH_MG5aMCatNLO_FTapprox/'
#path = '../LHE/HH_MG5_LO_forlambda/'
#path = '../LHE/HH_MG5aMCatNLO_FT/'
path='../hh_mc_gghh_lhe/'

if len(sys.argv) != 2:
  print("python my_HH_ana.py channel")
  exit(0)

# input: part as a branch of "Particle" from LHEF tree (ExRootTreeReader)
# return: per event a list of 4-vector [H1 H2 qg1 qg2] where qg is the NLO quark or gluon
# H1 H2, and qg1 qg2 are ordered by pT respectively
# empty element: fill None
def v4_each_event( part ):
  v4_hh = []
  v4_qg = []

  for ipart in part:
    if abs(ipart.PID) == 25:
      _part = R.TLorentzVector()
      _part.SetPxPyPzE( ipart.Px, ipart.Py, ipart.Pz, ipart.E )
      v4_hh.append( _part )
    elif ipart.Mother1 != ipart.Mother2:
      _part = R.TLorentzVector()
      _part.SetPxPyPzE( ipart.Px, ipart.Py, ipart.Pz, ipart.E )
      v4_qg.append( _part )

  if len(v4_hh) != 2:
    print("FOUND number of Higgs bosons != 2: {0}  EXIT".format(len(v4_hh)))
    exit(0)

  v4_hh.sort( key=lambda _p: _p.Pt(), reverse=True )
  #print([ _p.Pt() for _p in v4_hh ])

  v4_qg.sort( key=lambda _p: _p.Pt(), reverse=True )
  #print([ _p.Pt() for _p in v4_qg ])

  v4 = []
  v4 += v4_hh
  v4 += v4_qg
  v4 += ( [None]*(4-len(v4)) )

  return v4

def th1_gev( name, title, xmin, xmax ):
  dict_th1[name] = R.TH1F( name, title, xmax-xmin, xmin, xmax )

def th1( name, title, nbin, xmin, xmax ):
  dict_th1[name] = R.TH1F( name, title, nbin, xmin, xmax )

### MAIN ###

channel=sys.argv[1]
print('Process '+channel)

R.gSystem.Load('libExRootAnalysis.so')

# input LHE files
chain = R.TChain('LHEF')
chain.Add('{0}/{1}/*.root'.format(path,channel))
reader = R.ExRootTreeReader(chain)
nevt = reader.GetEntries()

# output hist files
outfile = R.TFile('{0}/{1}-hist.root'.format(path,channel), 'RECREATE')
dict_th1 = dict()

# histograms #
th1_gev('H1_pT','pp#rightarrowHH;p_{T}(H1) [GeV]',0,800)
th1    ('H1_eta','pp#rightarrowHH;#eta(H1)',100,-5,5)
th1    ('H1_phi','pp#rightarrowHH;#phi(H1)',100,-3.142, 3.142)
th1_gev('H1_E','pp#rightarrowHH;E(H1) [GeV]',0,1000)
th1    ('H1_m','pp#rightarrowHH;m(H1) [GeV]',100,125-0.05,125+0.05)

th1_gev('H2_pT','pp#rightarrowHH;p_{T}(H2) [GeV]',0,800)
th1    ('H2_eta','pp#rightarrowHH;#eta(H2)',100,-5,5)
th1    ('H2_phi','pp#rightarrowHH;#phi(H2)',100,-3.142, 3.142)
th1_gev('H2_E','pp#rightarrowHH;E(H2) [GeV]',0,1000)
th1    ('H2_m','pp#rightarrowHH;m(H2) [GeV]',100,125-0.05,125+0.05)

th1_gev('HH_pT','pp#rightarrowHH;p_{T}(HH) [GeV]',0,400)
th1    ('HH_eta','pp#rightarrowHH;#eta(HH)',100,-10,10)
th1    ('HH_phi','pp#rightarrowHH;#phi(HH)',100,-3.142, 3.142)
th1_gev('HH_E','pp#rightarrowHH;E(HH) [GeV]',0,2000)
th1_gev('HH_m','pp#rightarrowHH;m(HH) [GeV]',0,1000)

th1    ('HH_dEta','pp#rightarrowHH;#Delta#eta(HH)',100,0,8)
th1    ('HH_dPhi','pp#rightarrowHH;#Delta#phi(HH)',100,0,3.142)
th1    ('HH_dR','pp#rightarrowHH;#DeltaR(HH)',100,0,8)

th1_gev('gq_pT','pp#rightarrowHH;p_{T}(gq NLO ME) [GeV]',0,400)
th1    ('gq_eta','pp#rightarrowHH;#eta(gq NLO ME)',100,-10,10)
th1    ('gq_phi','pp#rightarrowHH;#phi(gq NLO ME)',100,-3.142, 3.142)
th1_gev('gq_E','pp#rightarrowHH;E(gq NLO ME) [GeV]',0,1000)

th1('weight','pp#rightarrowHH;MC weight',1000,-5,5)
# #

part = reader.UseBranch('Particle')
event = reader.UseBranch('Event')
for ientry in range(nevt):
  reader.ReadEntry(ientry)
  H1,H2,qg1,qg2 = v4_each_event(part)
  w = event[0].Weight
  #print(w)
  
  dict_th1['H1_pT']  .Fill(H1.Pt(),w)
  dict_th1['H1_eta'] .Fill(H1.Eta(),w)
  dict_th1['H1_phi'] .Fill(H1.Phi(),w)
  dict_th1['H1_E']   .Fill(H1.E(),w)
  dict_th1['H1_m']   .Fill(H1.M(),w)

  dict_th1['H2_pT']  .Fill(H2.Pt(),w)
  dict_th1['H2_eta'] .Fill(H2.Eta(),w)
  dict_th1['H2_phi'] .Fill(H2.Phi(),w)
  dict_th1['H2_E']   .Fill(H2.E(),w)
  dict_th1['H2_m']   .Fill(H2.M(),w)

  HH = H1 + H2
  dict_th1['HH_pT']  .Fill(HH.Pt(),w)
  dict_th1['HH_eta'] .Fill(HH.Eta(),w)
  dict_th1['HH_phi'] .Fill(HH.Phi(),w)
  dict_th1['HH_E']   .Fill(HH.E(),w)
  dict_th1['HH_m']   .Fill(HH.M(),w)

  dict_th1['HH_dEta'].Fill(abs(H1.Eta()-H2.Eta()),w)
  dict_th1['HH_dPhi'].Fill(H1.DeltaPhi(H2),w)
  dict_th1['HH_dR']  .Fill(H1.DeltaR(H2),w)

  qg = qg1
  if qg2 is not None:
    qg += qg2
  dict_th1['gq_pT']  .Fill(qg.Pt() if qg is not None else 0,w)
  dict_th1['gq_eta'] .Fill(qg.Eta() if qg is not None else 0,w)
  dict_th1['gq_phi'] .Fill(qg.Phi() if qg is not None else 0,w)
  dict_th1['gq_E']   .Fill(qg.E() if qg is not None else 0,w)

  dict_th1['weight'] .Fill(w,1)

outfile.Write()
outfile.Close()

