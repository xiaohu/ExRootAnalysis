# cvt lhe to root
# set pattern in mycvt_lhe_root.sh first !!!
#. mycvt_lhe_root.sh FTapprox-lambda01
#. mycvt_lhe_root.sh FT-lambda00
#. mycvt_lhe_root.sh FT-lambda01
#. mycvt_lhe_root.sh FT-lambda02.5
#. mycvt_lhe_root.sh FT-lambda05
#. mycvt_lhe_root.sh FT-lambda-05
#. mycvt_lhe_root.sh FT-lambda10
#. mycvt_lhe_root.sh FT-lambda-02.5
#. mycvt_lhe_root.sh FT-lambda-20
#. mycvt_lhe_root.sh FT-lambda-10
#. mycvt_lhe_root.sh FT-lambda15
#. mycvt_lhe_root.sh FT-lambda-15
#. mycvt_lhe_root.sh FT-lambda20
#. mycvt_lhe_root.sh FTapprox-lambda01 # HH_MG5aMCatNLO_FTapprox: change in mycvt_lhe_root.sh

# analysis root (from lhe)
# set path in my_HH_ana.py first !!!
#python my_HH_ana.py FTapprox-lambda01
#python my_HH_ana.py FT-lambda00
#python my_HH_ana.py FT-lambda01
#python my_HH_ana.py FT-lambda02.5
#python my_HH_ana.py FT-lambda05
#python my_HH_ana.py FT-lambda-05
#python my_HH_ana.py FT-lambda10
python my_HH_ana.py FT-lambda-02.5
python my_HH_ana.py FT-lambda-20
python my_HH_ana.py FT-lambda-10
python my_HH_ana.py FT-lambda15
python my_HH_ana.py FT-lambda-15
python my_HH_ana.py FT-lambda20
#python my_HH_ana.py FTapprox-lambda01
